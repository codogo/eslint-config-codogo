module.exports = {
	"extends": "react-app",

	"globals": {
		"R": true,
		"React": true,
		"Consts": true,
		"plog": true,
		"__DEV__": true,
		"graphql": true,
	},

	"plugins": [
		"react",
	],

	"rules": {
		"comma-dangle": ["warn", "always",],
		"guard-for-in": "off",
		"indent": ["warn", "tab",],
		"react/jsx-curly-spacing": ["warn", "always",],
		"react/jsx-equals-spacing": ["warn", "always",],
		"react/jsx-first-prop-new-line": ["warn", "multiline",],
		"react/jsx-indent-props": ["warn", "tab",],
		"react/jsx-indent": ["warn", "tab",],
		"space-infix-ops": ["warn",],

		"no-unused-vars": [
			"warn",
			{
				"argsIgnorePattern": "_+",
				"varsIgnorePattern": "_+",
				"ignoreRestSiblings": true,
			},
		],

		"object-property-newline": [
			"warn",
			{
				"allowMultiplePropertiesPerLine": true,
			},
		],

		"no-unused-expressions": [
			"warn",
			{
				"allowShortCircuit": true,
				"allowTernary": true,
				"allowTaggedTemplates": true,
			},
		],
	},
};
